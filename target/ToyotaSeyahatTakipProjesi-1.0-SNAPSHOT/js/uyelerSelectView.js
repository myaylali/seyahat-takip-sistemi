
// MENU OLARAK ÜYELER LİSTESİ SELECT İÇİN

var uyelerListeModel=Backbone.Model.extend({
    urlRoot:"/rest/uye"
});

var uyelerListeView = Backbone.View.extend({

    initialize: function () {
        this.render();
    },
    render: function () {
        ////VERİ GELME KISMI
        var uyeler=new uyelerListeModel();   // var uyeModel=new UModel({id:2});

        uyeler.fetch({                              //HTTP GET
            success:function (uyeler) {
                var i;
                for(i=0;i<Object.keys(uyeler.toJSON()).length;i++){        //DÖNGÜ İLE TÜMÜNÜ YAZDIRIR KENDİSİ HARİÇ

                    $('#uyeSec').append(
                        '<option id='+uyeler.toJSON()[i].id+' +  value='+uyeler.toJSON()[i].id+'>'+(i+1)+')'+uyeler.toJSON()[i].Ad+' '+uyeler.toJSON()[i].Soyad+'</option>'
                    );
                }

            },
            error:function (model,xhr,options) {
                console.log('Fetch error');
            }
        });

    }
});