////////////////UYE ID SINE GÖRE SEYAHATLER TABLOSU VİEW

var seyahatlerModel=Backbone.Model.extend({
    urlRoot:"/rest/seyahat"
});

var seyahatlerView = Backbone.View.extend({

    initialize: function () {
        this.render();
    },
    render: function () {
        var aktifUye=new aktifUyeModel();
        aktifUye.fetch({
            success:function (aktifuye) {
                var aktifUyeId = aktifuye.toJSON()[0].id;   //O ÜYEYE AİT SEYAHATLER GELİR

                var seyahatler=new seyahatlerModel({id:aktifUyeId});   // id=giriş yapan kullanıcının ID 'si


                seyahatler.fetch({                              //HTTP GET
                    success:function (seyahatler) {
                        var i;
                        $('#baslik').append(

                            '<th>No</th>'+
                            '<th>Bölümü</th>'+
                            '<th>Müdürü</th>'+
                            '<th>Başlangıç Tarihi</th>'+
                            '<th>Bitiş Tarihi</th>'+
                            '<th>Seyahat Yeri</th>'+
                            '<th>Gidiş Amacı</th>'+
                            '<th>Proje Kodu</th>'+
                            '<th>Maliyet</th>'+
                            '<th>İşlemler</th>'
                            
                        );
                        for(i=0;i<Object.keys(seyahatler.toJSON()).length-1;i++){        //DÖNGÜ İLE TÜMÜNÜ YAZDIRIR

                            $('#table-body').append(
                                '<tr>' +
                                '<td>'+(i+1)+'</td>' +
                                '<td>'+seyahatler.toJSON()[i].Bolumu+'</td>' +
                                '<td>'+seyahatler.toJSON()[i].Muduru+'</td>' +     //'<td>' + surfboard.get('stock') + '</td>'
                                '<td>'+seyahatler.toJSON()[i].seyahatBaslangici+'</td>' +
                                '<td>'+seyahatler.toJSON()[i].seyahatSonu+'</td>'+
                                '<td>'+seyahatler.toJSON()[i].seyahatYeri+'</td>' +
                                '<td>'+seyahatler.toJSON()[i].gidisAmaci+'</td>' +
                                '<td>'+seyahatler.toJSON()[i].projeKodu+'</td>' +
                                '<td>'+seyahatler.toJSON()[i].Maliyet+'</td>' +

                                '<td><a href="../pages/seyahatGuncelle.html?Id='+seyahatler.toJSON()[i].idSeyahat+'" role="button" class="btn btn-info" >Düzenle</a>' +
                                ' <a href="" class="btn btn-danger seyahatSilButton" data-id="'+seyahatler.toJSON()[i].idSeyahat+'" data-syeri="'+seyahatler.toJSON()[i].seyahatYeri+'" >Sil</a> '+
                                '</td>' +
                                '</tr>'
                            );
                        }

                    },
                    error:function (model,xhr,options) {
                        console.log('Fetch error');
                    }
                });
            }

        });
    }
});

////////////////UYARI MESAJI - SİL

$(document).on('click', '.seyahatSilButton', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var seyahatYeri = $(this).data('syeri');
    swal({
            title: "Seyahat yeri '"+seyahatYeri+"' olan seyahat silinsin mi ?",
            text: "Bu seyahate ait tüm bilgiler silinir!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Evet, sil!",
            closeOnConfirm: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '/rest/seyahat/sil/'+id,
                    data: id,
                    success: function (data) {
                        swal("Seyahat Silindi!", "Seyahate ait tüm veriler silindi", "success");
                        setTimeout(function(){
                            window.location.reload(1);
                        }, 2000)
                    },
                    error: function (data) {
                        swal("HATA!", "Silme işleminde hata meydana geldi.", "error");
                    }
                });
            } else {
                swal("İptal", "Silme işleminden vazgeçtin", "error");
            }
        });
});
