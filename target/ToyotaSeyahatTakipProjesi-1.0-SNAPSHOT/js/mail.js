var mailModel=Backbone.Model.extend({
    //urlRoot:"/rest/mail" // RESTFull kaynağı-URL si... urlRoot:model içinde bulunur amacı: backbone modelin idsyile beraber restfufl istekleri
    urlRoot:"/rest/mail/gonder/",
});


var mailView=Backbone.View.extend({ // Uygulama genelinde sayfanın genelinde event kullanılcaksa AppView oluşur

    el: $("body"), //tüm contexte erişen view olur
    events: {
        "click #btnMail":"gonder"
    },
    gonder:function (evt) {


        ///
        swal({
                title: "Mail Servisi",
                text: "Gönderilecek mail adresini giriniz:",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                animation: "slide-from-top",
                inputPlaceholder: "Mail adresi"
            },
            function(inputValue){
                if (inputValue === false) return false;

                if (inputValue === "") {
                    swal.showInputError("Lütfen mail adresi giriniz!");
                    return false;
                }
                if( !isValidEmailAddress(inputValue)){
                    swal.showInputError("Lütfen geçerli mail adresi giriniz!");
                    return false;
                }

                swal("Gönderildi!", "İletilen mail adresi: " + inputValue, "success");

                //////MAİL İÇERİĞİ
                var tab_text="<html><body><table border='2px'><tr>";
                var textRange;
                var j=0;
                var tab = document.getElementById('seyahatlerTablo'); // id of table

                for(j = 0 ; j < tab.rows.length ; j++)
                {
                    tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
                    //tab_text=tab_text+"</tr>";
                }

                tab_text=tab_text+"</table></body></html>";
                tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
                tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
                tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

                tab_text=tab_text.replace("İşlemler","");
                var icerik=tab_text;
                ////
                location='/rest/mail/gonder/'+inputValue+'/'+"İçerik yok";
            });
            ///

    }
});

var mView=new mailView();

function isValidEmailAddress(emailAddress) {
    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
    return pattern.test(emailAddress);
};