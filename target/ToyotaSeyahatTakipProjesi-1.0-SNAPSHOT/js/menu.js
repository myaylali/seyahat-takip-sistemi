var aktifUyeModel=Backbone.Model.extend({
    urlRoot:"/rest/uye/aktifUye"        //GİREN UYE BİLGİLERİ GELİR MODELDEN
});

var yetkiModel=Backbone.Model.extend({
    urlRoot:"/rest/yetki"
});

var MenuView = Backbone.View.extend({

    el: $('#menu'),
    initialize: function () {
        this.render();
    },
    render: function () {

        this.$el.html('<div class="container">' +
            '<div class="navbar-header">' +
            '   <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">' +
            '<span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>' +
            '   </button><a class="navbar-brand" href="/">Toyota Seyahat Takip</a></div><div id="navbar" class="navbar-collapse collapse">' +
            '<ul class="nav navbar-nav">'+
            '   <li><a href="/pages/seyahatler.html" id="seyahatLink"><div id="seyahatler"></div></a></li>'+
            '</ul>'+
            '<ul class="nav navbar-nav" >'+
            '   <li><a href="#" id="kLink"><div id="kullaniciAdi"></div></a></li>'+
            '</ul>'+
            '<ul class="nav navbar-nav navbar-right">'+
            '   <li><a href="/logout" id="cikis">Çıkış</a></li>'+
            '</ul>'+

            '</div>'+
            '</div>');

        //ÜYE BİLGİLERİ

        var aktifUye=new aktifUyeModel();       //MENUDE KULLANICI ADI VE GÜNCELLEME LİNKİ OLUŞTURULUYOR
        aktifUye.fetch({
            success:function (aktifuye) {
                var yetki=new yetkiModel({id:aktifuye.toJSON()[0].id});     //  KULLACININ YETKİSİ ALINIR

                yetki.fetch({
                    success:function (yetki) {

                        if(yetki.toJSON()[0].Yetki_id==1) {
                            $('#kullaniciAdi').html(aktifuye.toJSON()[0].Ad+" "+aktifuye.toJSON()[0].Soyad+" (Admin)");
                            document.getElementById("kLink").href="../pages/uyeGuncelle.html?Id="+aktifuye.toJSON()[0].id;
                            $('#seyahatler').html("Seyahatler");

                            sessionStorage.setItem('yetki', 'Admin');

                        }else{
                            $('#kullaniciAdi').html(aktifuye.toJSON()[0].Ad+" (Üye)");
                            document.getElementById("kLink").href="../pages/uyeGuncelle.html?Id="+aktifuye.toJSON()[0].id;
                            document.getElementById("seyahatLink").href="#";

                            sessionStorage.setItem('yetki', 'Uye');
                        }
                    }
                });
            },
            error:function (model,xhr,options) {
                console.log('Fetch error');
            }
        });
    }
});

var menuView = new MenuView();

$("#cikis").click(function(){
    sessionStorage.clear();
});