
//KULLANICININ YETKİ ID'Sİ ALINIYOR ONA GÖRE VİEW GÖSTERİLİYOR

var aktifUye=new aktifUyeModel();
    aktifUye.fetch({
        success:function (aktifuye) {
               //GİREN KULLANICININ ID SİNDEN YETKI ID Sİ ALINIR

            var aktifUyeId=aktifuye.toJSON()[0].id;

            var yetki=new yetkiModel({id:aktifUyeId});     //  KULLACININ YETKİSİ ALINIR

            yetki.fetch({
                success:function (yetki) {

                    if(yetki.toJSON()[0].Yetki_id==1) {     //YETKİ ADMİN(1) ise

                        var uyeView = new uyelerView();

                        $('#ekleButton').html("<a href='pages/uyeKayit.html'>"+
                            "<button type='button' class='btn btn-default btn-sm'>"+
                            "<span>+</span> Üye Ekle"+
                            "</button>"+
                            "</a>");


                        $('#araDiv').html('<input type="text" id="adminInput" onkeyup="uyeAra()" placeholder="Ad veya Soyad...">');

                        $('#anabaslik').html("ÜYELER");

                    }else{

                        var seyahatView=new seyahatlerView();   //YETKİ ÜYEYSE

                        $('#ekleButton').html("<a href='pages/seyahatKayit.html'>"+
                            "<button type='button' class='btn btn-default btn-sm'>"+
                            "<span>+</span> Seyahat Ekle"+
                            "</button>"+
                            "</a>");

                        $('#araDiv').html('<input type="text" id="uyeInput" onkeyup="seyahatAra()" placeholder="Seyahat yeri...">');


                        $('#anabaslik').html("SEYAHATLER");
                    }
                },
                error:function (model,xhr,options) {
                    console.log("Fetch error");
                }
            });
        },
        error:function (model,xhr,options) {
            console.log('Fetch error');
        }
});



