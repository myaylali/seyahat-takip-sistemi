var UModel=Backbone.Model.extend({
    urlRoot:"/rest/uye/kayit" // RESTFull kaynağı-URL si... urlRoot:model içinde bulunur amacı: backbone modelin idsyile beraber restfufl istekleri
});


var UView=Backbone.View.extend({ // Uygulama genelinde sayfanın genelinde event kullanılcaksa AppView oluşur

    el: $("body"), //tüm contexte erişen view olur
    events: {
        "click #uyeEkle-button":"kaydet"
    },
    kaydet:function (evt) {

        var uyeModel=new UModel();
        uyeModel.set('Ad',$(".Ad").val());   //FORMDAN GELEN VERİLER YOLLANIR VE EKLENİR
        uyeModel.set("Soyad",$(".Soyad").val());
        uyeModel.set("KullaniciAdi",$(".Kadi").val());
        uyeModel.set("Sifre",$(".Sifre").val());
        uyeModel.set("SicilNo",$(".sicilNo").val());
        uyeModel.set("Bolum",$(".bolum").val());
        uyeModel.set("BolumMuduru",$(".bolumMuduru").val());
        //uyeModel.set("YetkiAdi",$(".yetki").val());
        uyeModel.set("BilgilendirmeDurumu",$(".bilgiDurumu").val());
        uyeModel.set("BilgilendirmePeriyodu",$(".bilgiPeriyodu").val());
        uyeModel.set("BilgilendirmeGunu",$(".bilgiGunu").val());

        if($(".Ad").val()=="" || $(".Soyad").val()==""|| $(".Kadi").val()=="" || $(".Sifre").val()==""||$(".sicilNo").val()=="" || $(".bolum").val()=="" || $(".bolumMuduru").val()==""){
            $('.mesaj').hide();
            $('.uyari').show();
            $('#uyari').html("<img src='../resources/images/carpiTik.png' width='25' height='25'>"+' Lütfen tüm alanları doldurunuz!');

        }
        else {
            uyeModel.save();//HTTP POST
            //form gonderdikten sonra inputtaki yazılar silir
            $('.Ad').val('');
            $('.Soyad').val('');
            $('.Kadi').val('');
            $('.Sifre').val('');
            $('.sicilNo').val('');
            $('.bolum').val('');
            $('.bolumMuduru').val('');
            $('.bilgiDurumu').val('Kapalı');

            $('.mesaj').show();
            $('.uyari').hide();

            $('#mesaj').html("<img src='../resources/images/onayTik.png' width='25' height='25'>"+" Üye kayıt işlemi başarılı");

            swal("Başarılı !", "Üye kayıt işlemi tamamlandı", "success");
        }
    }
});

var uView=new UView();


function check() {

    var cat = document.getElementById("bilgiDurumu");
    if(cat.value == "Açık"){
        document.getElementById("bilgiPeriyodu").disabled = false;
        document.getElementById("bilgiGunu").disabled = false;
    }else{
        document.getElementById("bilgiPeriyodu").disabled = true;
        document.getElementById("bilgiGunu").disabled = true;
    }
}

//input girilen karakter kontrolleri

$(".Ad").keyup(function () {
    if (this.value.match(/[^a-zA-Z]/g)){
        this.value = this.value.replace(/[^a-zA-Z]/g,'');
    }
});

$(".Soyad").keyup(function () {
    if (this.value.match(/[^a-zA-Z]/g)){
        this.value = this.value.replace(/[^a-zA-Z]/g,'');
    }
});
$(".Kadi").keyup(function () {
    if (this.value.match(/[^a-zA-Z^0-9]/g)){
        this.value = this.value.replace(/[^a-zA-Z^0-9]/g,'');
    }
    this.value = this.value.toLowerCase();
});

$(".sicilNo").keyup(function () {
    if (this.value.match(/[^0-9]/g)){
        this.value = this.value.replace(/[^0-9]/g,'');
    }
});

$(".bolum").keyup(function () {
    if (this.value.match(/[^a-zA-Z]/g)){
        this.value = this.value.replace(/[^a-zA-Z]/g,'');
    }
});

$(".bolumMuduru").keyup(function () {
    if (this.value.match(/[^a-zA-Z]/g)){
        this.value = this.value.replace(/[^a-zA-Z]/g,'');
    }
});

$( document ).ready(function() {
    $('.mesaj').hide();
    $('.uyari').hide();
});


