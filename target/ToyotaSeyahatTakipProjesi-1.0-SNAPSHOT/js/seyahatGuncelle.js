var url = window.location.href; // or window.location.href for current url
var seyahatId = /Id=([^&]+)/.exec(url)[1]; // Value is in [1] ('384' in our case)



//BAŞLANGIÇTA INPUTLARDA GUNCELLENECEKSEYAHAT BİLGİLERİ GİRİLMESİ

var seyahatModel=Backbone.Model.extend({
    urlRoot:'/rest/seyahat/getir/'+seyahatId
});

var idUyeSbt;
var BolumuSbt;
var MuduruSbt;
var seyahatView = Backbone.View.extend({

    initialize: function () {
        this.render();
    },
    render: function () {

        var seyahat=new seyahatModel();   // var uyeModel=new UModel({id:2});

        seyahat.fetch({                              //HTTP GET
            success:function (seyahat) {

                $('.seyahatYeri').val(seyahat.toJSON()[0].seyahatYeri);
                $('.gidisAmaci').val(seyahat.toJSON()[0].gidisAmaci);
                $('.projeKodu').val(seyahat.toJSON()[0].projeKodu);
                $('.tarih1').val(seyahat.toJSON()[0].seyahatBaslangici);
                $('.tarih2').val(seyahat.toJSON()[0].seyahatSonu);
                $('.maliyet').val(seyahat.toJSON()[0].Maliyet);

                if(sessionStorage.getItem("yetki")=="Admin"){
                    $('#uyeSec').show();
                    $('#uyeSec').html(
                        '<option>'+seyahat.toJSON()[0].idUye+'</option>'    //AD SOYAD ÇEKİPTE GÖSTER
                    );
                    document.getElementById("uyeSec").disabled = true;

                }
                else{
                    $('#uyeSec').hide();
                    document.getElementById("maliyet").disabled=true;   //üye maliyeti değiştiremez
                }
                idUyeSbt=seyahat.toJSON()[0].idUye;
                BolumuSbt=seyahat.toJSON()[0].Bolumu;
                MuduruSbt=seyahat.toJSON()[0].Muduru;
            },
            error:function (model,xhr,options) {
                console.log('Fetch error');
            }
        });

    }
});

var seyahatView1 = new seyahatView();

/////////////////////////////////////////////////SEYAHAT GÜNCELLEME
var SModel=Backbone.Model.extend({
   urlRoot:"/rest/seyahat/guncelle/",
});

var SView=Backbone.View.extend({
    el:$("body"),
    events:{
        "click #guncelle-button":"guncelle"
    },
    guncelle:function (evt) {
        var smodel=new SModel();

        smodel.set('idUye',idUyeSbt);        //AYNI KALAN kISIMLAR
        smodel.set('Bolumu',BolumuSbt);
        smodel.set('Muduru',MuduruSbt);

        smodel.set('seyahatYeri',$('.seyahatYeri').val());   //FORMDAN GELEN VERİLER YOLLANIR VE EKLENİR
        smodel.set("gidisAmaci",$('.gidisAmaci').val());
        smodel.set("projeKodu",$('.projeKodu').val());
        smodel.set("seyahatBaslangici",$('.tarih1').val());
        smodel.set("seyahatSonu",$('.tarih2').val());

        smodel.set("Maliyet",$('.maliyet').val());

        if($(".seyahatYeri").val()=="" || $('.gidisAmaci').val()=="" || $('.projeKodu').val()=="" || $('.tarih1').val()=="" || $('.tarih2').val()=="" || $('.maliyet').val()==""){
            $('.mesaj').hide();
            $('.uyari').show();
            $('#uyari').html("<img src='../resources/images/carpiTik.png' width='25' height='25'>" + ' Lütfen tüm alanları doldurunuz!');

        }else{
            smodel.save({id:seyahatId});

            $('.mesaj').show();
            $('.uyari').hide();

            $('#mesaj').html("<img src='../resources/images/onayTik.png' width='25' height='25'>"+" Seyahat güncelleme işlemi başarılı");

            swal("Başarılı !", "Seyahat güncelleme işlemi başarılı", "success");
        }


    }
});

var sView2=new SView();
//////////////////////////

function tarihKontrol() {

    var tkvm1 = document.getElementById("datepicker1");
    var tkvm2 = document.getElementById("datepicker2");

    if(tkvm1.value > tkvm2.value){
        document.getElementById("guncelle-button").disabled = true;
        $('.mesaj').hide();
        $('.uyari').show();
        $('#uyari').html("<img src='../resources/images/carpiTik.png' width='25' height='25'>"+' Başlangıç tarihi bitiş tarihinden sonra olamaz!');
    }else{
        document.getElementById("guncelle-button").disabled = false;
        $('.uyari').hide();
    }

}

$(document).ready(function () {
    $('.mesaj').hide();
    $('.uyari').hide();
});

$("#iptal").click(function(){
    $(location).attr('href','/');
});

//input girilen karakter kontrolleri

$(".seyahatYeri").keyup(function () {
    if (this.value.match(/[^a-zA-Z]/g)){
        this.value = this.value.replace(/[^a-zA-Z]/g,'');
    }
});

$(".gidisAmaci").keyup(function () {
    if (this.value.match(/[^a-zA-Z]/g)){
        this.value = this.value.replace(/[^a-zA-Z]/g,'');
    }
});
$(".maliyet").keyup(function () {
    if (this.value.match(/[^0-9]/g)){
        this.value = this.value.replace(/[^0-9]/g,'');
    }
});