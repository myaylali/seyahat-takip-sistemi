var url = window.location.href; // url
var uyeId = /Id=([^&]+)/.exec(url)[1]; // urlden id alınır

/////////////////// başta UYE BİLGİLERİ İNPULARA GİRİLMESİ İÇİN GET ID

var uyeModel=Backbone.Model.extend({
    urlRoot:'/rest/uye/'+uyeId
});

var kadiTut;

var uyeView = Backbone.View.extend({

    initialize: function () {
        this.render();
    },
    render: function () {

        var uyeler=new uyeModel();   // var uyeModel=new UModel({id:2});

        uyeler.fetch({                              //HTTP GET
            success:function (uyeler) {

                $('.Ad').val(uyeler.toJSON()[0].Ad);
                $('.Soyad').val(uyeler.toJSON()[0].Soyad);
                $('.Kadi').val(uyeler.toJSON()[0].KullaniciAdi);
                $('.Sifre').val(uyeler.toJSON()[0].Sifre);
                $('.sicilNo').val(uyeler.toJSON()[0].SicilNo);
                $('.bolum').val(uyeler.toJSON()[0].Bolum);
                $('.bolumMuduru').val(uyeler.toJSON()[0].BolumMuduru);
                $('.bilgiDurumu').val(uyeler.toJSON()[0].BilgilendirmeDurumu);
                $('.bilgiPeriyodu').val(uyeler.toJSON()[0].BilgilendirmePeriyodu);
                $('.bilgiGunu').val(uyeler.toJSON()[0].BilgilendirmeGunu);
                $('.uyeDurumu').val(uyeler.toJSON()[0].UyeDurumu);


                kadiTut=$('.Kadi').val();

            },
            error:function (model,xhr,options) {
                console.log('Fetch error');
            }
        });

        var yetki=new yetkiModel({id:uyeId});
        yetki.fetch({
            success:function (yetki) {
                if(yetki.toJSON()[0].Yetki_id==1) {
                    $('#yetki').val("Admin");
                }else{
                    $('#yetki').val("Üye");
                }

            }
        });

    }
});

var uyeView1 = new uyeView();

/////////////////////////////// UYE GUNCELLEME

var UModel=Backbone.Model.extend({
    urlRoot:"/rest/uye/guncelle/" ,// RESTFull kaynağı-URL si... urlRoot:model içinde bulunur amacı: backbone modelin idsyile beraber restfufl istekleri
    url : function(){

        //Yetki id si belirleme
        var yetkiId;
        if($(".yetki").val()=="Admin"){
            yetkiId=1;
        }
        else{
            yetkiId=2;
        }

        var url = this.urlRoot + this.id+"/"+yetkiId;

        return url;
    }
});



var UView=Backbone.View.extend({

    el: $("body"), //tüm contexte erişen view olur
    events: {
        "click button":"guncelle"
    },
    guncelle:function (evt) {


        var uyeModel=new UModel({id:uyeId});      // var uyeModel=new UModel({id:uyeId,yetkiId:2});
        uyeModel.set('Ad',$(".Ad").val());   //FORMDAN GELEN VERİLER YOLLANIR VE EKLENİR
        uyeModel.set("Soyad",$(".Soyad").val());
        uyeModel.set("KullaniciAdi",$(".Kadi").val());
        uyeModel.set("Sifre",$(".Sifre").val());
        uyeModel.set("SicilNo",$(".sicilNo").val());
        uyeModel.set("Bolum",$(".bolum").val());
        uyeModel.set("BolumMuduru",$(".bolumMuduru").val());
        //uyeModel.set("YetkiAdi",$(".yetki").val());
        uyeModel.set("BilgilendirmeDurumu",$(".bilgiDurumu").val());
        uyeModel.set("BilgilendirmePeriyodu",$(".bilgiPeriyodu").val());
        uyeModel.set("BilgilendirmeGunu",$(".bilgiGunu").val());
        uyeModel.set("UyeDurumu",$(".uyeDurumu").val());

        if($(".Ad").val()=="" || $(".Soyad").val()==""|| $(".Kadi").val()=="" || $(".Sifre").val()==""||$(".sicilNo").val()=="" || $(".bolum").val()=="" || $(".bolumMuduru").val()==""){
            $('.mesaj').hide();
            $('.uyari').show();
            $('#uyari').html('Lütfen tüm alanları doldurunuz!!');
        }else {

            $('.mesaj').show();
            $('.uyari').hide();

            uyeModel.save();//HTTP POST

            delete uyeModel;

            $('#mesaj').html('ÜYE BİLGİLERİ BAŞARI İLE GÜNCELLENDİ ...  <a href="/">Anasayfa</a>');

            swal("Başarılı !", "Üye güncelleme işlemi başarılı", "success");

            if(kadiTut!=$('.Kadi').val()){

                $('#mesaj').html('Yeniden giriş yapmak için çıkışa yönlendiriliyorsunuz...');

                setTimeout('cikisagit()',2000);
            }

        }
    }
});


var uView1=new UView();

/// oto çıkış
function cikisagit() {
    sessionStorage.clear();
    window.location="/logout";
}

//////FORM'DA BİLGİ DURUMU
function check() {

    var bDurumu = document.getElementById("bilgiDurumu");
    if(bDurumu.value == "Açık"){
        document.getElementById("bilgiPeriyodu").disabled = false;
        document.getElementById("bilgiGunu").disabled = false;
        $('.bilgiPeriyodu').val('Haftalık');
        $('.bilgiGunu').val('Pazartesi');
    }else{
        document.getElementById("bilgiPeriyodu").disabled = true;
        document.getElementById("bilgiGunu").disabled = true;
    }
}

if(sessionStorage.getItem("yetki")=="Uye"){
    $('.uyeDurumu').hide();
    $('#yetkiSatir').hide();
}


        //input girilen karakter kontrolleri

$(".Ad").keyup(function () {
    if (this.value.match(/[^a-zA-Z]/g)){
        this.value = this.value.replace(/[^a-zA-Z]/g,'');
    }
});

$(".Soyad").keyup(function () {
    if (this.value.match(/[^a-zA-Z]/g)){
        this.value = this.value.replace(/[^a-zA-Z]/g,'');
    }
});
$(".Kadi").keyup(function () {
    if (this.value.match(/[^a-zA-Z^0-9]/g)){
        this.value = this.value.replace(/[^a-zA-Z^0-9]/g,'');
    }
    this.value = this.value.toLowerCase();
});

$(".sicilNo").keyup(function () {
    if (this.value.match(/[^0-9]/g)){
        this.value = this.value.replace(/[^0-9]/g,'');
    }
});

$(".bolum").keyup(function () {
    if (this.value.match(/[^a-zA-Z]/g)){
        this.value = this.value.replace(/[^a-zA-Z]/g,'');
    }
});

$(".bolumMuduru").keyup(function () {
    if (this.value.match(/[^a-zA-Z]/g)){
        this.value = this.value.replace(/[^a-zA-Z]/g,'');
    }
});

$( document ).ready(function() {
    $('.mesaj').hide();
    $('.uyari').hide();
});