package com.toyota.dao;

import com.toyota.model.UyeDurumu;
import com.toyota.model.Uyeler;
import org.hibernate.Criteria;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Repository;

import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import java.util.List;

@Repository
public class UyelerDAO {

    @Autowired
    private SessionFactory sessionFactory;

    //TÜM ÜYELER LİSTESİ-- GİRİŞ YAPAN HARİÇ
    public List<Uyeler> getAllUyeler(){

        User aktifUye= (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String kadi=aktifUye.getUsername();


        Session session=this.sessionFactory.getCurrentSession();

        Query query=session.createQuery("from Uyeler WHERE KullaniciAdi!=:kAdi");   // LİSTEDE KENDİNİ GÖRMEZ ADMİN. (Kendisini Silemez)
        query.setParameter("kAdi",kadi);
        List uyeList=query.list();

        return uyeList;
    }

    //// GİREN UYENIN BİLGİLERİ ALINIR MENU DE KULLANICI ADI GÖRÜNÜR
    public List<Uyeler> getAktifUye(){

        User aktifUye= (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String kadi=aktifUye.getUsername();


        Session session=this.sessionFactory.getCurrentSession();

        Query query=session.createQuery("from Uyeler WHERE KullaniciAdi =:kAdi");   // Girenin bilgileri alınır
        query.setParameter("kAdi",kadi);
        List uyeList=query.list();

        return uyeList;
    }

    //VERİLEN ID YE GÖRE UYE GELİR
    public List<Uyeler> getUye(int uyeId){

        Session session=this.sessionFactory.getCurrentSession();
        Query query=session.createQuery("from Uyeler WHERE id=:uyeId2");
        query.setParameter("uyeId2",uyeId);
        List uyeList=query.list();
        return uyeList;
    }

    public void setSessionFactory(SessionFactory sf){
        this.sessionFactory=sf;
    }

    public Uyeler uyeEkle(Uyeler uye){

        if(ismeGoreUyeBul(uye.getKullaniciAdi())!=null){    //Aynı k.adı var
                System.out.println("bu kullanıcıdan var");
                // HATA DÖNDÜR -- GİRİŞ EKRANINDAKİ GİBİ YAP!!!!!!!
        }

        if(uye.getBilgilendirmeDurumu().equals("Kapalı")){  //BİLGİLENDİRME KAPALIYSA DİĞER VERİLERİ "-- " olarak kaydet
            uye.setBilgilendirmeGunu("---");
            uye.setBilgilendirmePeriyodu("---");
        }
        uye.setUyeDurumu(UyeDurumu.AKTİF);

        Session session=this.sessionFactory.getCurrentSession();
        session.persist(uye);

        return uye;
    }

    @PUT
    public Uyeler uyeGuncelle(Uyeler uye,int id){

        uye.setId(id);

        if(uye.getBilgilendirmeDurumu().equals("Kapalı")){  //BİLGİLENDİRME KAPALIYSA DİĞER VERİLERİ "-- " olarak kaydet
            uye.setBilgilendirmeGunu("---");
            uye.setBilgilendirmePeriyodu("---");
        }


        Session session=this.sessionFactory.getCurrentSession();
        session.merge(uye);

        return uye;
    }

    @DELETE
    public Uyeler UyeSil(int id){
        Uyeler uye = new Uyeler();
        uye.setId(id);
        Session session=this.sessionFactory.getCurrentSession();
        session.delete(uye);

        //üyeye ait seyahatler silinir
        Query query = session.createQuery("delete Seyahatler where idUye = :ID");
        query.setParameter("ID", id);
        int result = query.executeUpdate();

        return uye;

    }

    /// SECURITY - GİRİŞ KONTROLÜ İÇİN KULLANICI BULMA //

    public Uyeler uyeBul(int uyeID){
        return (Uyeler)sessionFactory.getCurrentSession().get(Uyeler.class,uyeID);
    }
    public Uyeler ismeGoreUyeBul(String kadi){
        Criteria criteria=sessionFactory.getCurrentSession().createCriteria(Uyeler.class);
        criteria.add(Restrictions.eq("KullaniciAdi",kadi));
        return (Uyeler)criteria.uniqueResult();
    }
}
