package com.toyota.dao;

import com.toyota.model.UyeYetkileri;
import com.toyota.model.Uyeler;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import java.util.List;

@Repository
public class UyeYetkileriDAO {

    @Autowired
    private SessionFactory sessionFactory;

    UyeYetkileriDAO(){
    }

    public UyeYetkileri yetkiEkle(Uyeler uye){

        UyeYetkileri uyeYetkileri=new UyeYetkileri(uye.getId(),2);  //UYEYE OTOMATİK OLARAK 2(UYE) YETKİSİ ATANIR

        Session session=this.sessionFactory.getCurrentSession();
        session.persist(uyeYetkileri);

        return uyeYetkileri;
    }

    public UyeYetkileri yetkiGuncelle(Uyeler uye,int yetki_id){

        UyeYetkileri uyeYetkileri=new UyeYetkileri(uye.getId(),yetki_id);  //UYENİN YETKİSİ GÜNCELLENİR

        Session session=this.sessionFactory.getCurrentSession();
        session.persist(uyeYetkileri);

        return uyeYetkileri;
    }

    //ÜYE VE YETKİSİ GETİRİR
    public boolean getUyeveYetki(int uyeId){

        Session session=this.sessionFactory.getCurrentSession();
        Query query=session.createQuery("from UyeYetkileri WHERE uye_id=:uyeId2");
        query.setParameter("uyeId2",uyeId);

        List uyeList=query.list();

        //Eklemeyi her şekilde yapması gerektiği için false döner. Önce bi SQL select komutu çalışır

        return false;
    }

    public List<UyeYetkileri> getAktifUyeveYetki(int uyeId){

        Session session=this.sessionFactory.getCurrentSession();
        Query query=session.createQuery("from UyeYetkileri WHERE uye_id=:uyeId2");
        query.setParameter("uyeId2",uyeId);

        List uyeList=query.list();


        return uyeList;
    }

    /*

    @DELETE
    public UyeYetkileri YetkiSil(int id){

        UyeYetkileri uyeYetkileri=new UyeYetkileri();
        uyeYetkileri.setUye_id(id);
        Session session=this.sessionFactory.getCurrentSession();
        session.delete(uyeYetkileri);

        return uyeYetkileri;
    }
    */
}
