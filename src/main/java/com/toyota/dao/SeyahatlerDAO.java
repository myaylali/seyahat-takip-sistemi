package com.toyota.dao;

import com.toyota.model.Seyahatler;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import java.util.List;

@Repository
public class SeyahatlerDAO {
    @Autowired
    private SessionFactory sessionFactory;

    //GELEN ID YE GÖRE SEYAHATLER LİSTESİ GELİR
    @GET
    public List<Seyahatler> getSeyahatler(int uyeId){

        Session session=this.sessionFactory.getCurrentSession();
        Query query=session.createQuery("from Seyahatler WHERE idUye=:idUye2");
        query.setParameter("idUye2",uyeId);
        List seyahatList=query.list();

        return seyahatList;
    }
    //GELEN Seyahat id ye göre seyahat
    @GET
    public List<Seyahatler> getSeyahat(int seyahatId){

        Session session=this.sessionFactory.getCurrentSession();
        Query query=session.createQuery("from Seyahatler WHERE idSeyahat=:idSeyahat2");
        query.setParameter("idSeyahat2",seyahatId);
        List seyahatList=query.list();

        return seyahatList;
    }

    @GET
    public List<Seyahatler> getAllSeyahatler(){

        Session session=this.sessionFactory.getCurrentSession();
        Query query=session.createQuery("from Seyahatler ");
        List seyahatList=query.list();

        return seyahatList;
    }

    //Kayıt
    public Seyahatler seyahatEkle(Seyahatler seyahat){
        Session session=this.sessionFactory.getCurrentSession();
        session.persist(seyahat);

        return seyahat;
    }

    //Güncelleme
    public Seyahatler seyahatGuncelle(int id,Seyahatler seyahat){
        seyahat.setIdSeyahat(id);
        Session session=this.sessionFactory.getCurrentSession();
        session.merge(seyahat);
        return seyahat;
    }

    @DELETE
    public Seyahatler seyahatSil(int idSeyahat){
        Seyahatler seyahat=new Seyahatler();
        seyahat.setIdSeyahat(idSeyahat);

        Session session=this.sessionFactory.getCurrentSession();
        session.delete(seyahat);
        return seyahat;
    }


}
