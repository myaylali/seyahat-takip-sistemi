package com.toyota.model;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.persistence.*;

@JsonIgnoreProperties(ignoreUnknown=true)
@Entity
@Table(name="Seyahatler")
public class Seyahatler {
    @Id
    @Column(name="idSeyahat")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int idSeyahat;

    @Column(name="idUye")
    int idUye;

    @Column(name="Bolumu")
    String Bolumu;

    @Column(name="Muduru")
    String Muduru;

    @Column(name = "seyahatBaslangici")
    String seyahatBaslangici;

    @Column(name = "seyahatSonu")
    String seyahatSonu;

    @Column(name = "seyahatYeri")
    String seyahatYeri;

    @Column(name = "gidisAmaci")
    String gidisAmaci;

    @Column(name="projeKodu")
    String projeKodu;

    @Column(name="maliyet")
    String maliyet;

    public Seyahatler(){}

    public Seyahatler(int idSeyahat,int idUye, String bolumu, String muduru, String seyahatBaslangici, String seyahatSonu, String seyahatYeri, String gidisAmaci, String projeKodu, String maliyet) {
        super();
        this.idSeyahat=idSeyahat;
        this.idUye = idUye;
        this.Bolumu = bolumu;
        this.Muduru = muduru;
        this.seyahatBaslangici = seyahatBaslangici;
        this.seyahatSonu = seyahatSonu;
        this.seyahatYeri = seyahatYeri;
        this.gidisAmaci = gidisAmaci;
        this.projeKodu = projeKodu;
        this.maliyet = maliyet;
    }

    public int getIdSeyahat() {
        return idSeyahat;
    }

    public void setIdSeyahat(int idSeyahat) {
        this.idSeyahat = idSeyahat;
    }

    @JsonProperty("idUye")
    public int getIdUye() {
        return idUye;
    }

    public void setIdUye(int idUye) {
        this.idUye = idUye;
    }

    @JsonProperty("Bolumu")
    public String getBolumu() {
        return Bolumu;
    }

    public void setBolumu(String bolumu) {
        Bolumu = bolumu;
    }

    @JsonProperty("Muduru")
    public String getMuduru() {
        return Muduru;
    }

    public void setMuduru(String muduru) {
        Muduru = muduru;
    }

    @JsonProperty("seyahatBaslangici")
    public String getSeyahatBaslangici() {
        return seyahatBaslangici;
    }

    public void setSeyahatBaslangici(String seyahatBaslangici) {
        this.seyahatBaslangici = seyahatBaslangici;
    }

    @JsonProperty("seyahatSonu")
    public String getSeyahatSonu() {
        return seyahatSonu;
    }

    public void setSeyahatSonu(String seyahatSonu) {
        this.seyahatSonu = seyahatSonu;
    }

    @JsonProperty("seyahatYeri")
    public String getSeyahatYeri() {
        return seyahatYeri;
    }

    public void setSeyahatYeri(String seyahatYeri) {
        this.seyahatYeri = seyahatYeri;
    }

    @JsonProperty("gidisAmaci")
    public String getGidisAmaci() {
        return gidisAmaci;
    }

    public void setGidisAmaci(String gidisAmaci) {
        this.gidisAmaci = gidisAmaci;
    }

    @JsonProperty("projeKodu")
    public String getProjeKodu() {
        return projeKodu;
    }

    public void setProjeKodu(String projeKodu) {
        this.projeKodu = projeKodu;
    }

    @JsonProperty("Maliyet")
    public String getMaliyet() {
        return maliyet;
    }

    public void setMaliyet(String maliyet) {
        this.maliyet = maliyet;
    }
}
