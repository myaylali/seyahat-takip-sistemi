package com.toyota.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

@Entity
@Table(name="yetkiler")
public class Yetkiler implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private int id;

    private String yetkiAdi;

    @ManyToMany(mappedBy = "yetkiler")
    private Set<Uyeler> uyeler;


    public Yetkiler(){    }

    public Yetkiler(int id,String yetkiAdi, List<Uyeler> uyeler) {
        super();
        this.id=id;
        this.yetkiAdi = yetkiAdi;
        //this.uyeler = uyeler;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getYetkiAdi() {
        return yetkiAdi;
    }

    public void setYetkiAdi(String yetkiAdi) {
        this.yetkiAdi = yetkiAdi;
    }


    public Set<Uyeler> getUyeler() {
        return uyeler;
    }

    public void setUyeler(Set<Uyeler> uyeler) {
        this.uyeler = uyeler;
    }
}
