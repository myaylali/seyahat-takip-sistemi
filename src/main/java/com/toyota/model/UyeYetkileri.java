package com.toyota.model;



import org.codehaus.jackson.annotate.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;


@Entity
@Table(name="Uye_Yetkileri")
public class UyeYetkileri implements Serializable {


    @Id
    @Column(name="uye_id")
    private int uye_id;

    @Id
    @Column(name="yetki_id")
    private int yetki_id;

    public UyeYetkileri(){
    }

    public UyeYetkileri(int uye_id) {
        this.uye_id = uye_id;
    }

    public UyeYetkileri(int uye_id, int yetki_id) {
        this.uye_id = uye_id;
        this.yetki_id = yetki_id;
    }

    @JsonProperty("Uye_id")
    public int getUye_id() {
        return uye_id;
    }

    public void setUye_id(int uye_id) {
        this.uye_id = uye_id;
    }

    @JsonProperty("Yetki_id")
    public int getYetki_id() {
        return yetki_id;
    }

    public void setYetki_id(int yetki_id) {
        this.yetki_id = yetki_id;
    }


}
