package com.toyota.model;


import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name="Uyeler")
public class Uyeler implements Serializable{

    //@ManyToMany     //TABLO İLİŞKİLENDİRME
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinTable(name="uye_yetkileri",joinColumns = @JoinColumn(name = "uye_id"),inverseJoinColumns=@JoinColumn(name ="yetki_id") )
    @JsonIgnore
    private Set<Yetkiler> yetkiler;



    @Enumerated(EnumType.STRING)    //enum daki üye durumu
    private UyeDurumu UyeDurumu;


    @Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;

    @Column(name="Ad")
    String Ad;

    @Column(name = "Soyad")
    String Soyad;

    @Column(name = "KullaniciAdi")
    String KullaniciAdi;

    @Column(name = "Sifre")
    String Sifre;

    @Column(name = "SicilNo")
    String SicilNo;

    @Column(name = "Bolum")
    String Bolum;

    @Column(name = "BolumMuduru")
    String BolumMuduru;


    @Column(name = "BilgilendirmeDurumu")
    String BilgilendirmeDurumu;

    @Column(name = "BilgilendirmePeriyodu")
    String BilgilendirmePeriyodu;

    @Column(name = "BilgilendirmeGunu")
    String BilgilendirmeGunu;

    public Uyeler(){
        super();
    }


    public Uyeler(int i,String Ad,String Soyad,String kullaniciAdi,String sifre,String sicilNo,String bolum,String bolumMuduru,String BilgiDurumu,
                  String BilgiPeriyod,String BilgiGunu,UyeDurumu uyeDurumu){
        super();
        this.id=i;
        this.Ad=Ad;
        this.Soyad=Soyad;
        this.KullaniciAdi=kullaniciAdi;
        this.Sifre=sifre;
        this.SicilNo=sicilNo;
        this.Bolum=bolum;
        this.BolumMuduru=bolumMuduru;
        this.BilgilendirmeDurumu=BilgiDurumu;
        this.BilgilendirmePeriyodu=BilgiPeriyod;
        this.BilgilendirmeGunu=BilgiGunu;
        this.UyeDurumu=uyeDurumu;

    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @JsonProperty("Ad")         //JSON TÜRÜNDE GELEN VERİNİN PROPERTY SİNİ BELİRLER
    public String getAd() {
        return Ad;
    }

    public void setAd(String ad) {
        this.Ad = ad;
    }

    @JsonProperty("Soyad")
    public String getSoyad() {
        return Soyad;
    }

    public void setSoyad(String soyad) {
        Soyad = soyad;
    }

    @JsonProperty("KullaniciAdi")
    public String getKullaniciAdi() {
        return KullaniciAdi;
    }

    public void setKullaniciAdi(String kullaniciAdi) {
        KullaniciAdi = kullaniciAdi;
    }

    @JsonProperty("Sifre")
    public String getSifre() {
        return Sifre;
    }

    public void setSifre(String sifre) {
        Sifre = sifre;
    }

    @JsonProperty("SicilNo")
    public String getSicilNo() {
        return SicilNo;
    }

    public void setSicilNo(String sicilNo) {
        SicilNo = sicilNo;
    }

    @JsonProperty("Bolum")
    public String getBolum() {
        return Bolum;
    }

    public void setBolum(String bolum) {
        Bolum = bolum;
    }

    @JsonProperty("BolumMuduru")
    public String getBolumMuduru() {
        return BolumMuduru;
    }

    public void setBolumMuduru(String bolumMuduru) {
        BolumMuduru = bolumMuduru;
    }


    @JsonProperty("BilgilendirmeDurumu")
    public String getBilgilendirmeDurumu() {
        return BilgilendirmeDurumu;
    }

    public void setBilgilendirmeDurumu(String bilgilendirmeDurumu) {
        BilgilendirmeDurumu = bilgilendirmeDurumu;
    }

    @JsonProperty("BilgilendirmePeriyodu")
    public String getBilgilendirmePeriyodu() {
        return BilgilendirmePeriyodu;
    }

    public void setBilgilendirmePeriyodu(String bilgilendirmePeriyodu) {
        BilgilendirmePeriyodu = bilgilendirmePeriyodu;
    }

    @JsonProperty("BilgilendirmeGunu")
    public String getBilgilendirmeGunu() {
        return BilgilendirmeGunu;
    }

    public void setBilgilendirmeGunu(String bilgilendirmeGunu) {
        BilgilendirmeGunu = bilgilendirmeGunu;
    }

    public Set<Yetkiler> getYetkiler() {
        return yetkiler;
    }

    public void setYetkiler(Set<Yetkiler> yetkiler) {
        this.yetkiler = yetkiler;
    }

    @JsonProperty("UyeDurumu")
    public UyeDurumu getUyeDurumu() {
        return UyeDurumu;
    }

    public void setUyeDurumu(UyeDurumu uyeDurumu) {
        this.UyeDurumu = uyeDurumu;
    }


}


