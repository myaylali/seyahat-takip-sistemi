package com.toyota.service;

import com.toyota.dao.UyelerDAO;
import com.toyota.model.UyeDurumu;
import com.toyota.model.Uyeler;
import com.toyota.model.Yetkiler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;


@Service
public class UyeGirisService implements UserDetailsService {


    @Autowired
    private UyelerDAO uyeDao;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String kadi) throws UsernameNotFoundException {

        Uyeler uye = uyeDao.ismeGoreUyeBul(kadi);

        if(uye!=null){  //db de o kullanıcı varsa
            String sifre = uye.getSifre();
            //güvenlik bilgileri
            boolean enabled = uye.getUyeDurumu().equals(UyeDurumu.AKTİF);
            boolean accountNonExpired = uye.getUyeDurumu().equals(UyeDurumu.AKTİF);
            boolean credentialsNonExpired = uye.getUyeDurumu().equals(UyeDurumu.AKTİF);
            boolean accountNonLocked = uye.getUyeDurumu().equals(UyeDurumu.AKTİF);

            //Roller
            Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();

            for (Yetkiler yetki : uye.getYetkiler()) {  //Gİriş yapanın YETKİSİ BURADA ALINIR
                authorities.add(new SimpleGrantedAuthority(yetki.getYetkiAdi()));

            }
            //Spring security nesnesi
            org.springframework.security.core.userdetails.User securityUser = new
                    org.springframework.security.core.userdetails.User(kadi, sifre, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);

            if(!enabled) System.out.println("Bu hesap artık aktif değildir..");

            return securityUser;

        }else{

            System.out.println("HATA- Kullanıcı Adı Hatalı");
            throw new UsernameNotFoundException("User Not Found ! ");
        }

    }

}


