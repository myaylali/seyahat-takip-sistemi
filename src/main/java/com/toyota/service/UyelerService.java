package com.toyota.service;

import com.toyota.dao.SeyahatlerDAO;
import com.toyota.dao.UyeYetkileriDAO;
import com.toyota.dao.UyelerDAO;
import com.toyota.mailGonder;
import com.toyota.model.Seyahatler;
import com.toyota.model.Uyeler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.Timer;

@Service
@Path("/uye")
@XmlRootElement
public class UyelerService {

    @Autowired
    UyelerDAO uyelerDAO;

    @Autowired
    UyeYetkileriDAO uyeYetkileriDAO;

    @Autowired
    SeyahatlerDAO seyahatlerDAO;

    @Transactional
    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Uyeler> getAllUyeler(){
            return uyelerDAO.getAllUyeler();
    }


    @Transactional
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Uyeler> getUye(@PathParam("id") int id){
        return uyelerDAO.getUye(id);
    }

    @Transactional
    @GET
    @Path("/aktifUye")          //AKTİF UYE BİLGİLERİ
    @Produces(MediaType.APPLICATION_JSON)
    public List<Uyeler> getAktifUye(){
        return uyelerDAO.getAktifUye();
    }

    public String yetkiGetir(){
        User aktifUye= (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return aktifUye.getAuthorities().toString();

    }

    @Transactional
    @POST
    @Path("/kayit")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void uyeEkle(Uyeler uye){
        uyelerDAO.uyeEkle(uye);
        uyeYetkileriDAO.yetkiEkle(uye);

    }

    @Transactional
    @PUT
    @Path("/guncelle/{id}/{yetki_id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void uyeGuncelle(@PathParam("id") int id, @PathParam("yetki_id") int yetki_id, Uyeler uye){

       uyelerDAO.uyeGuncelle(uye,id);

        if(!uyeYetkileriDAO.getUyeveYetki(id)) {        //UYE GUNCELLENDİKTEN SONRA YETKİ SİLİNMESİ SORUNU VARDI: YETKİ EKLENİYOR
            uyeYetkileriDAO.yetkiGuncelle(uye,yetki_id);
        }

    }

    @Transactional
    @GET
    @Path("/sil/{id}")
    public String UyeSil(@PathParam("id") int id){

        uyelerDAO.UyeSil(id);



        return "<html><head><meta http-equiv='refresh' content='0;URL=http://localhost:8080'></head></html>";
    }


}
