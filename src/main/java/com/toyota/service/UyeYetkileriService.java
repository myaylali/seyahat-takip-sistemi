package com.toyota.service;

import com.toyota.dao.UyeYetkileriDAO;
import com.toyota.model.UyeYetkileri;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Service
@Path("/yetki")
public class UyeYetkileriService {
    @Autowired
    UyeYetkileriDAO uyeYetkileriDAO;

    @Transactional
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public List<UyeYetkileri> getUyeVeYetkiler(@PathParam("id") int id){
        return uyeYetkileriDAO.getAktifUyeveYetki(id);
    }

}
