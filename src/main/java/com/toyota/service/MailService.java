package com.toyota.service;

import com.toyota.mailGonder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Timer;

@Service
@Path("/mail")
@XmlRootElement
public class MailService {

    @Transactional
    @GET
    @Path("/gonder/{mailAdresi}/{icerik}")
    public void gonder(@PathParam("mailAdresi") String mailAdresi,@PathParam("icerik") String icerik){

        Timer time = new Timer(); // Timer nesnesi yarat
        mailGonder mg = new mailGonder(mailAdresi,icerik); // sinifimizi yarat
        time.schedule(mg, 0, 3600000); // saat basi calistir
        //time.schedule(mg, 0, 60000); // dakika basi calistir

    }
}
