package com.toyota.service;

import com.toyota.dao.SeyahatlerDAO;
import com.toyota.model.Seyahatler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Service
@Path("/seyahat")
@XmlRootElement
public class SeyahatlerService {
    @Autowired
    SeyahatlerDAO seyahatlerDAO;

    @Transactional
    @GET
    @Path("/{id}")  //üye id sine göre seyahatler
    @Produces(MediaType.APPLICATION_JSON)
    public List<Seyahatler> getSeyahatler(@PathParam("id") int id){ //id ye göre seyahat gelir)
        return seyahatlerDAO.getSeyahatler(id);
    }

    @Transactional
    @GET
    @Path("/getir/{id}") //seyahat id ye göre seyahat bilgilerini inputlara yazmak için GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Seyahatler> getSeyahat(@PathParam("id") int id){
        return seyahatlerDAO.getSeyahat(id);
    }

    @Transactional
    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Seyahatler> getAllSeyahatler(){ //tüm seyahatler gelir)
        return seyahatlerDAO.getAllSeyahatler();
    }

    @Transactional
    @POST
    @Path("/kayit")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void seyahatEkle(Seyahatler seyahat){
        seyahatlerDAO.seyahatEkle(seyahat);
    }


    @Transactional
    @PUT
    @Path("/guncelle/{idSeyahat}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public void seyahatGuncelle(@PathParam("idSeyahat") int idSeyahat,Seyahatler seyahat){
        seyahatlerDAO.seyahatGuncelle(idSeyahat,seyahat);
    }

    @Transactional
    @GET
    @Path("/sil/{id}")
    public String seyahatSil(@PathParam("id") int id){
        seyahatlerDAO.seyahatSil(id);

        User aktifUye= (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        System.out.println(aktifUye.getAuthorities().toString());

        if(aktifUye.getAuthorities().toString().equals("[Uye]")) {
            return "<html><head><meta http-equiv='refresh' content='0;URL=http://localhost:8080'></head></html>";
        }
        else {
            return "<html><head><meta http-equiv='refresh' content='0;URL=http://localhost:8080/pages/seyahatler.html'></head></html>";
        }
    }

}
