////////////////UYELER TABLOSU VİEW

var uyelerModel=Backbone.Model.extend({
    urlRoot:"/rest/uye"
});

var uyelerView = Backbone.View.extend({

    initialize: function () {
        this.render();
    },
    render: function () {
        ////VERİ GELME KISMI
        var uyeler=new uyelerModel();   // var uyeModel=new UModel({id:2});

        uyeler.fetch({                              //HTTP GET
            success:function (uyeler) {
                var i;
                $('#baslik').append(

                            '<th>id</th>'+
                            '<th>Ad Soyad</th>'+
                            '<th>Bölüm</th>'+
                            '<th>Bölüm Müdürü</th>'+
                            '<th>Durum</th>'+
                            '<th>Rolü</th>'+
                            '<th>Seyahat Bilgilendirme</th>'+
                            '<th>İşlemler</th>'

                );

                var indis=0;

                for(i=0;i<Object.keys(uyeler.toJSON()).length;i++){        //DÖNGÜ İLE TÜMÜNÜ YAZDIRIR

                    //DENEME YETKİ
                    var yetki=new yetkiModel({id:uyeler.toJSON()[i].id});
                    yetki.fetch({
                        success:function (yetki) {
                            if(yetki.toJSON()[0].Yetki_id==1) {
                                $('#yetki' + indis).html("Admin");
                            }else{
                                $('#yetki' + indis).html("Üye");
                            }
                            indis++;
                        }
                    });
                    //

                    $('#table-body').append(
                        '<tr>' +
                        '<td>'+uyeler.toJSON()[i].id+'</td>' +
                        '<td>'+uyeler.toJSON()[i].Ad+' '+uyeler.toJSON()[i].Soyad+'</td>' +
                        '<td>'+uyeler.toJSON()[i].Bolum+'</td>' +     //'<td>' + surfboard.get('stock') + '</td>'
                        '<td>'+uyeler.toJSON()[i].BolumMuduru+'</td>' +
                        '<td>'+uyeler.toJSON()[i].UyeDurumu+'</td>'+
                        '<td id="yetki'+i+'"></td>'+
                        '<td>'+uyeler.toJSON()[i].BilgilendirmeDurumu+" / "+uyeler.toJSON()[i].BilgilendirmePeriyodu+'</td>' +
                        '<td><a href="../pages/uyeGuncelle.html?Id='+uyeler.toJSON()[i].id+'" role="button" class="btn btn-info" >Düzenle</a>' +
                        //' <a href="rest/uye/sil/'+uyeler.toJSON()[i].id+'" role="button" class="btn btn-danger" >Sil</a>' +
                        ' <a href="" class="btn btn-danger silButton" data-id="'+uyeler.toJSON()[i].id+'" data-ad="'+uyeler.toJSON()[i].Ad+'" data-soyad="'+uyeler.toJSON()[i].Soyad+'">Sil</a> '+
                        '</td>' +
                        '</tr>'
                    );
                }

            },
            error:function (model,xhr,options) {
                console.log('Fetch error');
            }
        });

    }
});


////////////////SweetAlert- UYARI MESAJI - SİL

$(document).on('click', '.silButton', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var ad = $(this).data('ad');
    var soyad = $(this).data('soyad');
    swal({
            title: "'"+ad+' '+soyad+"' silinsin mi ?",
            text: ad+" adına olan tüm seyahat bilgileri silinir!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Evet, sil!",
            closeOnConfirm: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: 'rest/uye/sil/'+id,
                    data: id,
                    success: function (data) {
                       swal("Üye Silindi!", "Üyeye ait tüm veriler silindi", "success");
                        setTimeout(function(){
                            window.location.reload(1);
                        }, 2000)
                    },
                    error: function (data) {
                        swal("HATA!", "Silme işleminde hata meydana geldi.", "error");
                    }
                });
            } else {
                swal("İptal", "Silme işleminden vazgeçtin", "error");
            }
        });
});
