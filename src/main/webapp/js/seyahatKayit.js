var uyeModel=Backbone.Model.extend({
    urlRoot:"/rest/uye"
});

var seyahatKayitModel=Backbone.Model.extend({
    urlRoot:"/rest/seyahat/kayit" // RESTFull kaynağı-URL si... urlRoot:model içinde bulunur amacı: backbone modelin idsyile beraber restfufl istekleri
});

var seyahatKayitView=Backbone.View.extend({ // Uygulama genelinde sayfanın genelinde event kullanılcaksa AppView oluşur

    el: $("body"), //tüm contexte erişen view olur
    events: {
        "click #seyahatEkle-button":"kaydet"
    },
    kaydet:function (evt) {

        var seyahatModel=new seyahatKayitModel();
        var aktifUye=new aktifUyeModel();
        aktifUye.fetch({
            success:function (aktifuye) {
                if(sessionStorage.getItem("yetki")=="Admin"){       //admin ekliyorsa üyenin id belirler
                    var aktifUyeId=  $('#uyeSec').val();
                    if( aktifUyeId=="hicbiri"){
                        $('.mesaj').hide();
                        $('.uyari').show();
                        $('#uyari').html("<img src='../resources/images/carpiTik.png' width='25' height='25'>"+' Lütfen kullanıcı seçiniz!');
                    }else {

                        var uye = new uyeModel({id: aktifUyeId});
                        uye.fetch({
                            success: function (uye) {
                                var aktifUyeBolumu = uye.toJSON()[0].Bolum;
                                var aktifUyeMuduru = uye.toJSON()[0].BolumMuduru;
                                seyahatModel.set('Bolumu', aktifUyeBolumu);
                                seyahatModel.set('Muduru', aktifUyeMuduru);
                                seyahatModel.set('Maliyet', $('.maliyet').val());

                                if ($(".seyahatYeri").val() == "" || $('.gidisAmaci').val() == "" || $('.projeKodu').val() == "" || $('.tarih1').val() == "" || $('.tarih2').val() == " ") {
                                }
                                else {
                                    seyahatModel.save();//HTTP POST

                                    $('.seyahatYeri').val('');
                                    $('.gidisAmaci').val('');
                                    $('.projeKodu').val('');
                                    $('.tarih1').val('');
                                    $('.tarih2').val('');
                                    $('.maliyet').val('');
                                }


                            }
                        });
                    }
                }
                else{                                               //uye ekliyorsa bellidir.
                    var aktifUyeId = aktifuye.toJSON()[0].id;
                    document.getElementById("uyeSec").value=aktifUyeId;
                    var aktifUyeBolumu=aktifuye.toJSON()[0].Bolum;
                    var aktifUyeMuduru=aktifuye.toJSON()[0].BolumMuduru;
                    seyahatModel.set('Maliyet',"Belirlenmedi");
                }


                seyahatModel.set('idUye',aktifUyeId);
                seyahatModel.set('Bolumu',aktifUyeBolumu);
                seyahatModel.set('Muduru',aktifUyeMuduru);

                seyahatModel.set('seyahatYeri',$(".seyahatYeri").val());   //FORMDAN GELEN VERİLER YOLLANIR VE EKLENİR
                seyahatModel.set("gidisAmaci",$(".gidisAmaci").val());
                seyahatModel.set("projeKodu",$(".projeKodu").val());
                seyahatModel.set("seyahatBaslangici",$(".tarih1").val());
                seyahatModel.set("seyahatSonu",$(".tarih2").val());

                if($(".seyahatYeri").val()=="" || $('.gidisAmaci').val()=="" || $('.projeKodu').val()=="" || $('.tarih1').val()=="" || $('.tarih2').val()==" " || $('#uyeSec').val()=="hicbiri"){
                    $('.mesaj').hide();
                    $('.uyari').show();
                    if($('#uyeSec').val()!="hicbiri") {
                        $('#uyari').html("<img src='../resources/images/carpiTik.png' width='25' height='25'>" + ' Lütfen tüm alanları doldurunuz!');
                    }
                }
                else{
                    if(sessionStorage.getItem("yetki")=="Uye"){ // Admin ise uye bolum ve muduru fetch ederken post edilir
                        seyahatModel.save();

                        $('.seyahatYeri').val('');
                        $('.gidisAmaci').val('');
                        $('.projeKodu').val('');
                        $('.tarih1').val('');
                        $('.tarih2').val('');
                        $('.maliyet').val('');
                    }

                    $('.mesaj').show();
                    $('.uyari').hide();

                    $('#mesaj').html("<img src='../resources/images/onayTik.png' width='25' height='25'>"+" Seyahat ekleme işlemi başarılı");

                    swal("Başarılı !", "Seyahat ekleme işlemi başarılı", "success");


                }

            }});
    }
});

var sView=new seyahatKayitView();


function tarihKontrol() {


    var tkvm1 = document.getElementById("datepicker1");
    var tkvm2 = document.getElementById("datepicker2");

    if(tkvm1.value > tkvm2.value){
        document.getElementById("seyahatEkle-button").disabled = true;
        $('.mesaj').hide();
        $('.uyari').show();
        $('#uyari').html("<img src='../resources/images/carpiTik.png' width='25' height='25'>"+' Başlangıç tarihi bitiş tarihinden sonra olamaz!');
    }else{
        document.getElementById("seyahatEkle-button").disabled = false;
        $('.uyari').hide();
    }

}


$("#iptal").click(function(){
    $(location).attr('href','/');
});

$(document).ready(function () {

    $('.mesaj').hide();
    $('.uyari').hide();

    if(sessionStorage.getItem("yetki")=="Admin"){
        $('#uyeSec').show();
        ////MENU OLARAK ÜYELER LİSTESİ SELECT İÇİN --  üyeler listelenir
        var uyelerListesi=new uyelerListeView();

        $('#maliyetSatir').show();
    }
    else{
        $('#uyeSec').hide();

        $('#maliyetSatir').hide();
    }
});

//input girilen karakter kontrolleri

$(".seyahatYeri").keyup(function () {
    if (this.value.match(/[^a-zA-Z]/g)){
        this.value = this.value.replace(/[^a-zA-Z]/g,'');
    }
});

$(".gidisAmaci").keyup(function () {
    if (this.value.match(/[^a-zA-Z]/g)){
        this.value = this.value.replace(/[^a-zA-Z]/g,'');
    }
});

$(".maliyet").keyup(function () {
    if (this.value.match(/[^0-9]/g)){
        this.value = this.value.replace(/[^0-9]/g,'');
    }
});