////MENU OLARAK ÜYELER LİSTESİ SELECT İÇİN  uyeSec iteminde üyeler listelenir
var uyelerListesi=new uyelerListeView();

//Seçilen üyeye göre view gösterilir
function uyeSeyahatGoster() {

    $('#table-body').html(' ');
    var seyahat=new seyahatlerView();
}

//////SEYAHATLER
////////////////UYE ID SINE GÖRE SEYAHATLER TABLOSU VİEW
var uyeModel=Backbone.Model.extend({
    urlRoot:"/rest/uye"        //GİREN UYE BİLGİLERİ GELİR MODELDEN
});

var seyahatlerModel=Backbone.Model.extend({
    urlRoot:"/rest/seyahat"
});

var seyahatlerView = Backbone.View.extend({

    initialize: function () {
        this.render();
    },
    render: function () {

              var secilenUye = $('#uyeSec').val();

              if(secilenUye=="hepsi"){
                  var seyahatler = new seyahatlerModel();
              }else {
                  var seyahatler = new seyahatlerModel({id: secilenUye});
              }
                seyahatler.fetch({                              //HTTP GET
                    success:function (seyahatler) {

                        var i;
                        $('#baslik').html(
                            '<tr>'+
                                '<th>Seyahat Eden</th>'+
                                '<th>Bölümü</th>'+
                                '<th>Müdürü</th>'+
                                '<th>Başlangıç Tarihi</th>'+
                                '<th>Bitiş Tarihi</th>'+
                                '<th>Seyahat Yeri</th>'+
                                '<th>Gidiş Amacı</th>'+
                                '<th>Proje Kodu</th>'+
                                '<th>Maliyet</th>'+
                                '<th>İşlemler</th>'+
                            '</tr>'
                        );

                        var indis=0;

                        for(i=0;i<Object.keys(seyahatler.toJSON()).length;i++){        //DÖNGÜ İLE TÜMÜNÜ YAZDIRIR
                            var uyeIdsi=seyahatler.toJSON()[i].idUye;

                            var uye = new uyeModel({id: uyeIdsi});
                            uye.fetch({success:function (uye) {
                                $('#adi'+indis).html(
                                   uye.toJSON()[0].Ad+" "+uye.toJSON()[0].Soyad);      //uye ıd sine ait kullanıcı alınır adı ve soyadı gösterilir
                                indis++;
                            }});

                            $('#table-body').append(
                                '<tr>' +
                                '<td id="adi'+i+'" ></td>' +
                                '<td>'+seyahatler.toJSON()[i].Bolumu+'</td>' +
                                '<td>'+seyahatler.toJSON()[i].Muduru+'</td>' +
                                '<td>'+seyahatler.toJSON()[i].seyahatBaslangici+'</td>' +
                                '<td>'+seyahatler.toJSON()[i].seyahatSonu+'</td>'+
                                '<td>'+seyahatler.toJSON()[i].seyahatYeri+'</td>' +
                                '<td>'+seyahatler.toJSON()[i].gidisAmaci+'</td>' +
                                '<td>'+seyahatler.toJSON()[i].projeKodu+'</td>' +
                                '<td>'+seyahatler.toJSON()[i].Maliyet+'</td>' +

                                '<td><a href="../pages/seyahatGuncelle.html?Id='+seyahatler.toJSON()[i].idSeyahat+'" role="button" class="btn btn-info" >Düzenle</a>' +
                                ' <a href="" class="btn btn-danger seyahatSilButton" data-id="'+seyahatler.toJSON()[i].idSeyahat+'" data-syeri="'+seyahatler.toJSON()[i].seyahatYeri+'" >Sil</a> '+
                                '</td>' +
                                '</tr>'
                            );
                        }
                    }
                });
            }
});

////////////////SEYAHATLER TABLOSU VİEW SON

$( document ).ready(function() {
    uyeSeyahatGoster();
});


////////////////UYARI MESAJI - SİL

$(document).on('click', '.seyahatSilButton', function (e) {
    e.preventDefault();
    var id = $(this).data('id');
    var seyahatYeri = $(this).data('syeri');
    swal({
            title: "Seyahat yeri '"+seyahatYeri+"' olan seyahat silinsin mi ?",
            text: "Bu seyahate ait tüm bilgiler silinir!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Evet, sil!",
            closeOnConfirm: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    url: '/rest/seyahat/sil/'+id,
                    data: id,
                    success: function (data) {
                        swal("Seyahat Silindi!", "Seyahate ait tüm veriler silindi", "success");
                        setTimeout(function(){
                            window.location.reload(1);
                        }, 2000)
                    },
                    error: function (data) {
                        swal("HATA!", "Silme işleminde hata meydana geldi.", "error");
                    }
                });
            } else {
                swal("İptal", "Silme işleminden vazgeçtin", "error");
            }
        });
});