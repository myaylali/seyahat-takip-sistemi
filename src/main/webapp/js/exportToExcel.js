function fnExcelReport()
{
    var tab_text="<table border='2px'><tr bgcolor='#87AFC6'>";
    var textRange;
    var j=0;
    var tab = document.getElementById('seyahatlerTablo'); // id of table

    for(j = 0 ; j < tab.rows.length ; j++)
    {
        tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text=tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    tab_text=tab_text.replace("İşlemler","");

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    //tarih bilgileri
    var dt = new Date();
    var day = dt.getDate();
    var month = dt.getMonth() + 1;
    var year = dt.getFullYear();
    var hour = dt.getHours();
    var mins = dt.getMinutes();
    var tarih = day + "." + month + "." + year + "_" + hour + "." + mins;

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus();
        sa=txtArea1.document.execCommand("SaveAs",true,"SeyahatlerRapor.xls");
    }
    else {                 //other browser not tested on IE 11

        var universalBOM = "\uFEFF";

        uriContent='data:application/vnd.ms-excel; charset=utf-8,' + encodeURIComponent(universalBOM+tab_text);

        var sa = document.createElement("a");

        sa.setAttribute("href", uriContent);
        sa.setAttribute("download", "SeyahatlerRapor_"+tarih+".xls");
        document.body.appendChild(sa); // Required for FF

        sa.click();

    }
    return (sa);
};
